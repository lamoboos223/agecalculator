from flask import Flask, request
import datetime

# done creating flask app
app= Flask(__name__)
now = datetime.datetime.now()

# create endpoint

#  /age
@app.route('/age', methods=['POST'])
def calculateAge():
    year = request.form.get('year')
    age = now.year - int(year)
    return str(age)
    

# copy it as is
if __name__ == '__main__':
    app.run(debug=True,
            port=8080,
            host="127.0.0.1")

